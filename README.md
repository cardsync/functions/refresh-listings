# Functions - Refresh Listings

This is a Google Cloud Function that initiates a scan of ebay listings that need to be refreshed. It's triggered by a Cloud Scheduler action in GCP.

EBay listings will eventually expire if they don't change after a while. This makes sure user's listing ids stay constant for their inventory.
