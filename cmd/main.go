package main

import (
	"context"

	refreshlistings "gitlab.com/cardsync/functions/refresh-listings"
)

func main() {
	refreshlistings.RefreshListings(context.Background(), refreshlistings.PubSubMessage{})
}
