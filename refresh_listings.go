package refreshlistings

import (
	"context"
	"database/sql"
	"os"

	"cloud.google.com/go/pubsub"
	_ "github.com/lib/pq"

	kitlog "github.com/go-kit/kit/log"
)

type PubSubMessage struct {
	Data []byte `json:"data"`
}

var (
	psql   *sql.DB
	logger kitlog.Logger

	topic *pubsub.Topic
)

func init() {
	var err error

	{
		logger = kitlog.NewJSONLogger(os.Stdout)
		logger = kitlog.With(logger, "timestamp", kitlog.DefaultTimestampUTC, "transport", "pubsub")
	}

	{
		psql, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			logger.Log("level", "fatal", "msg", "could not connect to database", "err", err)
			os.Exit(1)
		}
		logger.Log("level", "debug", "msg", "connected to database", "url", os.Getenv("DATABASE_URL"))
	}

	// Only allow 1 connection to the database to avoid overloading it.
	psql.SetMaxIdleConns(1)
	psql.SetMaxOpenConns(1)

	{
		client, err := pubsub.NewClient(context.Background(), "synkt-1556838469966")
		if err != nil {
			logger.Log("level", "fatal", "msg", "error creating pubsub client", "err", err)
			os.Exit(1)
		}

		topic = client.Topic("refresh-listing")
	}
}

func RefreshListings(ctx context.Context, m PubSubMessage) error {
	var (
		ids  []string = make([]string, 0)
		rows *sql.Rows

		err error
	)

	logger.Log("level", "debug", "msg", "received message")

	rows, err = psql.Query(
		`select offer_id
		from user_card_conditions
		join ebay_offers on user_card_conditions.id = user_card_condition_id
		join user_ebays on user_ebays.user_id = user_card_conditions.user_id
		where user_card_conditions.quantity = 0
			and (refreshed_at is null OR refreshed_at < now() - interval '60 days')`,
	)
	if err != nil {
		return err
	}

	for err == nil && rows.Next() {
		var id string

		err = rows.Scan(&id)
		if err != nil {
			continue
		}

		ids = append(ids, id)
	}

	for _, id := range ids {
		_, err = topic.Publish(ctx, &pubsub.Message{
			Data: []byte(id),
		}).Get(ctx)
		if err != nil {
			logger.Log("level", "error", "msg", "error sending pubsub message", "err", err)
		}
	}

	return err
}
