module gitlab.com/cardsync/functions/refresh-listings

go 1.11

require (
	cloud.google.com/go/pubsub v1.0.1
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/lib/pq v1.2.0
)
